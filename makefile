.PHONY: all
all: strlen

strlen: src/strlen.cpp
	g++ -o strlen -O2 -Wall -Wextra -pedantic -std=c++17 src/strlen.cpp
	strip --strip-all strlen


.PHONY: install
install:
	mkdir -p "$(DESTDIR)/usr/bin"
	install -m755 -s strlen "$(DESTDIR)/usr/bin/"

.PHONY: uninstall
uninstall:
	rm "$(DESTDIR)/usr/bin/strlen"

.PHONY: clean
clean:
	rm -f strlen
